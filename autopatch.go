package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
)

const oldname = "linux-xanmod"
const newname = "linux-xanmod-anbox"

type repl struct {
	Old string
	New string
}

var replacements = []repl{
	{
		oldname,
		newname,
	},
	{
		"Linux Xanmod",
		"Linux Xanmod with ashmem and binder enabled for Anbox",
	},
	{
		"The Linux kernel and modules with Xanmod patches",
		"The Linux kernel and modules with Xanmod patches and ashmem and binder enabled",
	},
	{
		"Header files and scripts for building modules for Xanmod Linux kernel",
		"Header files and scripts for building modules for Xanmod Linux kernel with ashmem and binder enabled",
	},
}

const anboxpatch = `
  # Anbox compatibility
  msg2 "Enabling ashmem and binder modules"
  scripts/config --enable CONFIG_ASHMEM
  scripts/config --enable CONFIG_ANDROID
  scripts/config --enable CONFIG_ANDROID_BINDER_IPC
  scripts/config --enable CONFIG_ANDROID_BINDERFS
  scripts/config --set-str CONFIG_ANDROID_BINDER_DEVICES "binder,hwbinder,vndbinder"
`

func run(command string, args ...string) error {
	printedName := command
	for _, a := range args {
		printedName += " " + a
	}
	fmt.Printf("Running \"%s\"\n", printedName)
	cmd := exec.Command(command, args...)
	by, e := cmd.CombinedOutput()
	fmt.Println(string(by))
	// fmt.Printf("\"%s\" exited with exit code %d\n\n", printedName, cmd.ProcessState.ExitCode())
	return e
}

func runWithOutput(command string, args ...string) ([]byte, error) {
	printedName := command
	for _, a := range args {
		printedName += " " + a
	}
	fmt.Printf("Running \"%s\"\n", printedName)
	cmd := exec.Command(command, args...)
	by, e := cmd.CombinedOutput()
	fmt.Println(string(by))
	// fmt.Printf("\"%s\" exited with exit code %d\n\n", printedName, cmd.ProcessState.ExitCode())
	return by, e
}

func removeIfExists(filename string) error {
	if _, e := os.Stat(filename); e == nil {
		return os.RemoveAll(filename)
	}
	return nil
}

func main() {
	pub := flag.Bool("publish", false, "publish to AUR")
	flag.Parse()

	fmt.Printf("Freshly cloning %s\n", oldname)
	e := removeIfExists(oldname)
	if e != nil {
		panic(e)
	}
	e = run("git", "clone", fmt.Sprintf("https://aur.archlinux.org/%s.git", oldname), "./"+oldname)
	if e != nil {
		panic(e)
	}

	fmt.Println("Patching names and descriptions")
	e = replace(fmt.Sprintf("%s/PKGBUILD", oldname))
	if e != nil {
		panic(e)
	}
	e = replace(fmt.Sprintf("%s/.SRCINFO", oldname))
	if e != nil {
		panic(e)
	}

	fmt.Println("Enabling ashmem and binder")
	e = patch(fmt.Sprintf("%s/PKGBUILD", oldname))
	if e != nil {
		panic(e)
	}

	if *pub {
		fmt.Println("Publishing to AUR")
		e = submit()
		if e != nil {
			panic(e)
		}
	}
}

func replace(filename string) error {
	by, e := ioutil.ReadFile(filename)
	if e != nil {
		return e
	}
	str := string(by)
	for _, r := range replacements {
		str = strings.ReplaceAll(str, r.Old, r.New)
	}
	e = ioutil.WriteFile(filename, []byte(str), os.ModePerm)
	return e
}

type stage int

const (
	start stage = iota
	preparefound
	scriptsfound
	done
)

func patch(filename string) error {
	by, e := ioutil.ReadFile(filename)
	if e != nil {
		return e
	}
	str := string(by)

	index := 0
	stg := start

	for stg != done {
		switch stg {
		case start:
			const prep = "prepare()"
			if str[index:index+len(prep)] == prep {
				stg = preparefound
				for str[index] != '\n' {
					index++
				}
			}
		case preparefound:
			const scriptsconf = "scripts/config"
			if str[index:index+len(scriptsconf)] == scriptsconf {
				for str[index] != '\n' {
					if str[index] == '\\' {
						index++
					}
					index++
				}
				stg = scriptsfound
			}
		case scriptsfound:
			injected := inject(str, index)
			return ioutil.WriteFile(filename, []byte(injected), os.ModePerm)
		}

		index++
		if index >= len(str) {
			return fmt.Errorf("place to put Anbox patches not found")
		}
	}
	return fmt.Errorf("unknown error")
}

func inject(pkgbuild string, index int) string {
	return pkgbuild[:index] + anboxpatch + pkgbuild[index:]
}

func submit() error {
	e := removeIfExists(newname)
	if e != nil {
		return e
	}

	e = run("git", "clone", fmt.Sprintf("ssh://aur@aur.archlinux.org/%s.git", newname), "./"+newname)
	if e != nil {
		return e
	}

	e = removeIfExists(fmt.Sprintf("%s/.git", oldname))
	if e != nil {
		return e
	}

	e = run("cp", "-r", fmt.Sprintf("%s/.", oldname), newname)
	if e != nil {
		return e
	}

	e = run("sh", "-c", fmt.Sprintf("cd %s && git add .", newname))
	if e != nil {
		return e
	}

	by, e := runWithOutput("sh", "-c", fmt.Sprintf("cd %s && git commit -m \"automatic update\"", newname))
	if e != nil {
		str := string(by)
		if !strings.Contains(str, "nothing to commit") {
			return e
		}
	}

	e = run("sh", "-c", fmt.Sprintf("cd %s && git push", newname))
	if e != nil {
		return e
	}

	return nil
}
